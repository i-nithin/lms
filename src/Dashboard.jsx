import React, { useEffect, useRef, useState } from 'react';
import { OverlayPanel } from 'primereact/overlaypanel';
import { Menu } from 'primereact/menu';
import { Toast } from 'primereact/toast';
import DashboardContainer, {
  TopBar, SideBar, Container, Body, Footer,
} from '@ims/dashboard';
import http from './lib/utils/httpClient';

function Dashboard() {
  const menu = useRef(null);
  const toast = useRef(null);
  const [username, setUsername] = useState('');
  const menuItems = [
    {
      label: username,
      items: [
        { label: 'Logout', icon: 'pi pi-fw pi-sign-out', url: '/logout' },
      ],
    },
  ];

  const showAlert = ({ type, msg }) => {
    toast.current.show({
      severity: type,
      summary: type.toUpperCase(),
      detail: msg,
    });
  };

  useEffect(() => {
    (async () => {
      try {
        const response = await http.get('/auth/user/', {
          headers: {
            Authorization: `Token ${window.localStorage.getItem('AUTH_TOKEN')}`,
          },
        });
        if (response.status === 200) {
          setUsername(response.data?.username);
        }
      } catch (error) {
        showAlert({
          type: 'error',
          msg: 'Error in obtaining user data',
        });
      }
    })();
  }, []);

  return (
    <DashboardContainer>
      <TopBar>
        <a href="/" className="layout-topbar-logo">
          <span>{ process.env.APP_TITLE }</span>
        </a>

        <div className="layout-topbar-menu">
          <button type="button" className="p-link layout-topbar-button" onClick={(e) => menu.current.toggle(e)}>
            <i className="pi pi-user" />
            <span>Profile</span>
          </button>
          <OverlayPanel ref={menu}>
            <Menu model={menuItems} />
          </OverlayPanel>
        </div>
      </TopBar>
      <SideBar />
      <Container>
        <Body />
        <Footer />
      </Container>
      <Toast ref={toast} />
    </DashboardContainer>
  );
}

export default Dashboard;
